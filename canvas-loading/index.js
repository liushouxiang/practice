var F = 0;
function Loading() {
    this.canvas = document.getElementById('loading');
    this.ctx = this.canvas.getContext('2d');
    this.progress = 0;
    this.delta = 0;
    this.raf = null;
    this.progressBar = new ProgressBar(this);
    this.runner = new Runner(this);
    this.render();
}

Loading.prototype = {
    construct: Loading,

    setProgress: function(progress) {
        this.delta = progress - this.progress;
        this.progress = progress;
    },

    renderBackground: function() {
        var ctx = this.ctx, 
            w = this.canvas.width,
            h = this.canvas.height,
            r = h / 2;

        ctx.clearRect(0, 0, w, h);
        ctx.beginPath();
        ctx.fillStyle = 'rgb(252,228,155)';
        ctx.arc(r, r, r, - Math.PI / 2, Math.PI / 2, true);
        ctx.lineTo(w - r, 2 * r);
        ctx.arc(w -r , r, r, Math.PI / 2, -Math.PI / 2, true);
        ctx.lineTo(r, 0);
        ctx.fill();
    },

    render: function() {
        this.renderBackground();
        this.progressBar.draw(this.progress, this.delta);
        this.runner.draw(this.progress, this.delta);
        this.raf = window.requestAnimationFrame( (function(me) {
            return function() {
                me.render();
            };
        })(this) );
    },

    stop: function() {
        window.cancelAnimationFrame(this.raf);
        this.raf = null;
    }
};

function ProgressBar(parent) {
    this.pctx = parent.ctx;
    this.canvas = document.createElement('canvas');
    this.canvas.width = this.width = 214;
    this.canvas.height = this.height = 34;
    this.ctx = this.canvas.getContext('2d');
    this.currentWidth = 0;
    this.leaves = [];
}

ProgressBar.prototype.draw = function(progress, delta) {
    var ctx = this.ctx, 
        w = this.width,
        h = this.height,
        pw = this.pctx.canvas.width,
        ph = this.pctx.canvas.height,
        r = h / 2,
        speed = this.getSpeed(progress, delta);

    this.currentWidth += speed;
    ctx.clearRect(0, 0, w, h);
    ctx.beginPath();
    ctx.arc(r, r, r, - Math.PI / 2, Math.PI / 2, true);
    ctx.lineTo(w - r, 2 * r);
    ctx.arc(w -r , r, r, Math.PI / 2, -Math.PI / 2, true);
    ctx.lineTo(r, 0);
    ctx.clip();

    ctx.fillStyle = 'rgb(255,168,0)';
    ctx.fillRect(0, 0, this.currentWidth, h);
    this.drawLeaves(progress, delta);
    this.pctx.drawImage(this.canvas, (pw - w) / 2, (ph - h) / 2 );
};

ProgressBar.prototype.drawLeaves = function(progress, delta) {
    var now = Date.now(), i = 0;
    while (this.leaves.length > i) {
        if (now - this.leaves[i].startTime > 3000) {
            this.leaves.splice(i, 1);
        } else {
            i ++;
        }
    }
    for (i=0;i< 10 - this.leaves.length;i++) {
        this.leaves.push(new Leaf(this));
    }
    this.ctx.save();
    this.ctx.globalCompositeOperation = 'destination-over';
    this.ctx.translate(this.width - this.height / 2, this.height / 2);
    this.ctx.rotate(Math.PI);
    this.leaves.forEach(function (leaf) {
        if (leaf.startTime <= now) {
            leaf.draw();
        }
    });
    this.ctx.rotate(- Math.PI);
    this.ctx.translate(this.height / 2 - this.width, - this.height / 2);
    this.ctx.restore();
};

ProgressBar.prototype.getSpeed = function(progress, delta) {
    var distance = this.width * progress - this.currentWidth;
    if (distance <= 0) {
        return 0;
    }
    return delta * this.width / 20;
};

var leafImage = new Image();
leafImage.src = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAwAAAAMCAYAAABWdVznAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyBpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuMC1jMDYwIDYxLjEzNDc3NywgMjAxMC8wMi8xMi0xNzozMjowMCAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENTNSBXaW5kb3dzIiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOjE3MTMzREQ4RDUyRjExRTRBMkM1Q0FFRjYxNEM4QkE1IiB4bXBNTTpEb2N1bWVudElEPSJ4bXAuZGlkOjE3MTMzREQ5RDUyRjExRTRBMkM1Q0FFRjYxNEM4QkE1Ij4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6MTcxMzNERDZENTJGMTFFNEEyQzVDQUVGNjE0QzhCQTUiIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6MTcxMzNERDdENTJGMTFFNEEyQzVDQUVGNjE0QzhCQTUiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz6sHiD2AAAA3klEQVR42mL882Q2Awpg4nBj+P2Zn+HNnlDGP7+4kKX+C1luY0FX/P/jKQeWi5MrGbCAP3yKl1igCkEkXsVwM+EsoDMIKUbR8P/bdUMGIgAT1DkMTO+vWhNSzMgi8QRhw98/zPgU/5W2O8rAKfaMieHfD2JcwvBfRP8Q0DUbYE5yw6f4j35uOyO/2QGQ4SxgxcAQYn56yBrdCYxsfB/+S3gtZGTl/QhUvAskzsLw9ZEK89nqqWBFqkGrGfiMD4B1sAm+/s/KuxrMRnI2C8w0kBvB1jIw7ILLYvEfQIABAOXHUd6jTluYAAAAAElFTkSuQmCC';


function Leaf(parent) {
    this.pctx = parent.ctx;
    this.canvas = document.createElement('canvas');
    this.ctx = this.canvas.getContext('2d');
    this.canvas.width = 12;
    this.canvas.height = 12;
    this.A = Math.max(1.5, Math.random() * parent.canvas.height / 2); // 振幅
    this.speed = Math.PI * 0.8; //运行速度
    this.func = Math.random() > 0.5 ? 'sin' : 'cos';
    this.x = 0;
    this.y = null;
    this.deg = 0;
    this.startTime = Date.now() + Math.ceil(Math.random() * 10000);
    this.clockwise = Math.random() > 0.5 ? 1 : -1;
};

Leaf.prototype.draw = function () {
    this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);
    this.ctx.save();
    this.y = Math[this.func](this.x / 20) * this.A - 6;
    this.ctx.translate(6, 6);
    this.ctx.rotate(this.deg);
    this.ctx.drawImage(leafImage, -6, -6);
    this.pctx.drawImage(this.canvas, this.x, this.y, 12, 12);
    this.deg += this.clockwise * Math.PI / 80;
    this.x += this.speed;
    this.ctx.restore();
}

function Runner(parent) {
    this.parent = parent;
    this.pctx = parent.ctx;
    this.canvas = document.createElement('canvas');
    this.ctx = this.canvas.getContext('2d');
    this.d = 46;
    this.canvas.width = this.d;
    this.canvas.height = this.d;
    this.deg = 0;
    this.speed = Math.PI / 20;
    this.ratio = 1;
}

var runnerImage = new Image();
runnerImage.src = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAC8AAAAvCAYAAABzJ5OsAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyBpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuMC1jMDYwIDYxLjEzNDc3NywgMjAxMC8wMi8xMi0xNzozMjowMCAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENTNSBXaW5kb3dzIiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOjdEN0VCMkEyRDUyRjExRTQ4RjYxQTQzMEQ0OTA0OUJBIiB4bXBNTTpEb2N1bWVudElEPSJ4bXAuZGlkOjdEN0VCMkEzRDUyRjExRTQ4RjYxQTQzMEQ0OTA0OUJBIj4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6N0Q3RUIyQTBENTJGMTFFNDhGNjFBNDMwRDQ5MDQ5QkEiIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6N0Q3RUIyQTFENTJGMTFFNDhGNjFBNDMwRDQ5MDQ5QkEiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz5Co4uvAAAJv0lEQVR42qRaXWwcVxU+987sev3vpA1NU0JTGgEVpfxYspBAQoDyAKoaVTyVF6SKCiFRkQf+HvqCECJIPCDxI1Cr5gFoRPlpSyNqQUIEammpFKpGraKQQhIbJ04a4sTx2uvdmbmc78zcmTuzO/aMM9Jox7szc8/57nfO+c699nunHqWbPXT0P/mM9C0/3uxetbrwFdW7QaFS5HkNipRHmszgm03ED+j4Ux52rvFn8N/H61sb9uzVCWqMj+mx+/akLxy7t1n2mFl5oytOrpw8R70bK+raX34WdpbnPe3NkhIYYgPLjC8CEZz/6VbATo3eyNgqBxyKLh05apb/8UsTdA5jNkod6EO+nvGlRrMRQJRh7RCtXR789PA7GNwW8fOYoT4nwoUnHjfLp494Ss8SnCg6EDC9PFPb+BP8Ml9PzLzPNVoMXnqJzOop0stn+eUrFPrlE+FFbEQUUNSaIBq5k9TIPaS3fSznSLj41L/o4jMHo2DlkGcdcGfCOlPReEHb2/n59+SMvnSEeKqJpzp+kd8aHMwmTK8RnPJd2KWQDfDCgMzQFNHENOnb7jfshOqbBcSD9mPUfVWLNn2GMzJkLv5GjOYXc4Zp1M9OjkNhEvzaHyO6/cGIx0phDv998I/R1b/+yKPmrBjuoI7Dr2o40GY0GO3TcYpjpKMy45Tq+y4ypn8G2AlLjZAp5507pMP2nPHu/pa8gD8/Kz9eOd7jKTpWTJe6quFm/vtiOOghuTkx0DD6OLXW6Un4zT0xkP2d/7bP4j3WEQ5SCj2f1NvHVHD6QBYn7EA0dc+D1FtnD/O0GWw8B2eOKox4uHJFDBdDeXAgKdTptpm7zPtwPT77cqHJPh30gTq4r7urROttol5HYgAO6KU3KefAHV98hEamHgmpGzuwAedPcAa412YVvMQiDsNVxIOaHkWje0lNzkjaS+nAmUctHeWLMEU8NRx/W+MjNqI5Rmbyk8QznL4DqVZd+QNRe4lCzNL2TwD5NJUGp76+ny9nLdWKxucMD+d/TtHis3nDG0Nkbn2AeGbKig6Zs99hr3kW+JlBhpvJjyO79OV6J1DZiePx++78QjoW2/NPdeH33+BacQz5Pk8b5nlqOLLKldnUcI+nWHLznsdKDU/kAZmpT/GoQfJF3nDMGNAsMzzhOTHPpSao+V8LIPL97i99hOnzbqEPgMyh7mqUpReE0xbx0GuCexsO6oDAIw3lOB4TPX5HlQP3STFD4eOYS2d2x/6veUG3ELAF1BGg1BzNEG29s5rheCk4DF4iM1jagC7jM5XfIffxTCM16munUvQF4KldBzi3Zsbzl3e5qIMuXiJVIVnV+H2VixAGjho7+2gDntc5IB8ETA5eiz4ANs337wMwOkmNr+N7G3C0uiR0CUll2kK3tjSwy/XaB+jHCQI1AOkzRf+2+/dFQ2MPxcbrVos9kmoLzeKWfM+mPG4gakkAFlxpmox45vydlSnj0g+2QAch/yMVJ+g3tN8c0ymPLGLdN/sNhw2sHGshD0OtKgT3IYdrHsj7KGReoipdG6Lm7R/UxRwddtdiw8F3K0VR0tcX0mmrPPjQHXFB8rzatBN7rr+aV5GrC9n15Mw+LTrG8RRpEXzPdS84kS4hzGo4kPKe06ZbiSt1mlyo9I0z5DYlhoLc+H6u7+QuCKWftV5/wwtNszYnDtTK9yFLhYZfpR2MOzGOLWlurOEYH0UygvLsyD0ejw2q+8XGOgwhU0EZr7/xTRwwZx6jaOKjRKPvGtjS5d8ZxFJhM2O7F0m1r7LBrCxBM7eLKmnAB0KCFOm57Zf7MCjFv6rrLxIt+2RaL3AevkWyiXUmPdpz/N12HqUpkhr3pUslXCUVnOJTSSrm5DA0Onj5o+Qon89Nlh3IH45/YzmrDdLoOXbmFebl08nzrEuY6+rub8vMQJ0iVwPZLJ0xEP7QJoFTFXmeKs9DcRrQ9G5pNcpPKYKZMTt25vEJFoUu1Gs7xpbjiZRpisZDK0vQanRJjVQWlKKPvhOqEeIrmWou2TnaiLpMWke9yrPCMWK1eR9NoWA5LYoj6ysxhawTydgoVCJZHFrijmk+121FM/5zrES5w9EDpiwRWtHIHtE6emImK0hlhQpHh2k1sYGChMTm0y6lSEMTrMUOODYohyhYdfP7tDjWXdj4kPO6p7082q1xMts+Q/4Ger6YUSwdQZEqVRlp0LC0QIDT2lIsfJPWTzW8DJD23Fs6XTu0HnFG0C5t7CcHltr9zQ0bkYG0BxV8XzJM1QIHAzGWUcNxS2nBGP6QU21fe1YnxanjlF3hs47yPagZ+XBtYRULunaMXqeesJOxJqZj4LjuoDm3khoxaoIo0AklAruCKwHXGGWqFYKUg3ErwoqCIC44vbg61jowJpZEQNnJTJVyXLyh1q8dsvlw2lIn7UF7nZtPlShSTBmRxYifurIakhjtJ8cgYs1RuC9nzUis11dS9KHFh8aYbkHG+5oDp/Ka+1bpprA4VVNWx7WAQRzfkaZgtjHgYvc0FnTdSpRHf8d+EWm2has7sASnZIusYCHf11Gl0fKr8WLsrQ+klOG68QQ3Jn+T7iofXBn6yCrSuqFxNomi5GJSWdI6HX+2ahbUA+DGSQbx09m6zeJTZxj1w7Y5KWqAaTdtyjLF8DZSZj12As1BFcPZSTgbFxmVNeJJm1npHReelPTqrpjRxWe+B9TL1yoZfVngd/ItNYbjm3na0SRsRhesPkgHVdRH7IyCeNuEOlipU9ePx2NbCl06chQZpsqeVG7Zz64S28VWrKcMakiAuBgOrg9adLLrlJyKEVNuwbP6Xp6HYWx4yvMLT55U53/1ZQ7Sv1vKIAFstLnQ54CIrPZb8YRhjb61O9PnWO211XDQGmVRboP/TMnIanx0TqgF2z9A/nt/mNvmieZ/8VVZYHV6aszqpjsjrgPplF5+Puttc01KyfJ2aTqJU7HoKMjdXZ8jb9fDLu9PqrnDj9rs4hpeeU+q6EA2C/+R6idLztrPI128dmcF16iaqCM+z+DY3j4aYktHXf7zd3NUKYBUfTdwwBampE5U0eUTDOKKoIeKaLd2kGIV9YiVeCz2mCrSI0MroTmHdmEJUOC+bKZJIXIRHzC79fZhvcbrxe3MXDPNjoj0XT3PcdDLIS+rcMn2pd1Q6At4pom59PxBbCbbzqmUklvcAR+4J3sTO+A9dvysevu5H2ArH4GZGm6SdsMzJcZv/X8PUidyaz/VDIY1IefuP6mrLx8KqfvbHNIuTcoacLSFN/tfH8l/fBzglLeXm+y7ktZSJ5/utSf7aMHiNbW68DsuOD+JG9Fmnhq2b95c+dD/BRgA8WlNQBDY5QAAAAAASUVORK5CYII=';

Runner.prototype.draw = function(progress, delta) {
    var pw = this.parent.canvas.width,
        ph = this.parent.canvas.height,
        r = this.d / 2;

    this.speed = Math.max(delta * 5 * Math.PI, Math.PI / 20);
    this.ctx.clearRect(0, 0, this.d, this.d);
    this.ctx.save();
    this.ctx.translate(r, r);
    this.ctx.rotate(this.deg);
    this.ctx.drawImage(runnerImage, -r, -r, this.d, this.d);
    this.ctx.restore();
    this.pctx.drawImage(this.canvas, pw - ph, 0);
    this.deg += this.speed;
};

// var testCtx = document.getElementById('test').getContext('2d');
// deg = 0;
// function render() {
//     window.requestAnimationFrame(function() {
//         testCtx.clearRect(0, 0, 300, 300);
//         testCtx.save();
//         testCtx.translate(150, 150);
//         testCtx.rotate(deg);
//         testCtx.drawImage(leafImage, -6, -6);
//         testCtx.restore();
//         deg += Math.PI / 100;
//         render();
//     });
// }
// render();