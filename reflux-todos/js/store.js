import Reflux from 'reflux'
import Actions from './action.js'

const storage = localStorage
const STORERAGE_NAME = 'todos'
const genId = () => (new Date()).getTime()

let TodoStore = Reflux.createStore({

    listenables: [Actions],

    getInitialState () {
        this.fetch()
        return this.list
    },

    addItem (text, id = genId(), completed = false) {
        this.list = [{text, id, completed}, ...this.list]
        this.save()
    },

    removeItem (id) {
        let index = this.list.findIndex(item => item.id === id)
        if (index !== -1) {
            this.list.splice(index, 1)
            this.save()
            return true
        }
        return false
    },

    updateItem ({id, text, completed = false}) {
        let item = this.list.find(item => item.id === id)
        if (item) {
            text && (item.text = text)
            completed && (item.completed = completed)
            this.save()
            return true
        }
        return false
    },

    toggleItem (id) {
        let item = this.list.find(item => item.id === id)
        if (item) {
            item.completed = !item.completed
            this.save()
            return true
        }
        return false
    },

    clearCompletedItems () {
        this.list = this.list.filter(item => !item.completed)
        this.save()
    },

    toggleAll (completed) {
        this.list.forEach(item => item.completed = completed)
        this.save()
    },

    save () {
        storage.setItem(STORERAGE_NAME, JSON.stringify(this.list))
        this.trigger(this.list)
    },

    fetch () {
        this.list = JSON.parse(storage.getItem(STORERAGE_NAME)) || []
    }

})

export default TodoStore