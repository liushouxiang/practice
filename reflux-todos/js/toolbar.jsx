import React, {Component} from 'react'
import {Link} from 'react-router'
import classNames from 'classnames'
import TodoActions from './action.js'
import {decorate as mixin} from 'react-mixin'

@mixin(TodoActions)
export default class Toolbar extends Component{

    onClear () {
        this.clearCompletedItems()
    }
    
    render () {

        let clearBtn = null

        if (this.props.completedItemsCount) {
            clearBtn = (
                <button
                    className="clear-completed"
                    onClick={this.onClear.bind(this)}
                >Clear completed</button>
            )
        }

        return (
            <footer className="footer">
                <span className="todo-count">
                    <strong>{this.props.remainItemsCount}</strong>
                    <span> </span>
                    <span>items </span>
                    <span>left</span>
                </span>
                <ul className="filters">
                    <li>
                        <Link activeClassName="selected" to='/'>All</Link>
                    </li>
                    <li>
                        <Link activeClassName="selected" to='/active'>Active</Link>
                    </li>
                    <li>
                        <Link activeClassName="selected" to='/completed'>Completed</Link>
                    </li>
                </ul>
                {clearBtn}
            </footer>
        )
    }

}
