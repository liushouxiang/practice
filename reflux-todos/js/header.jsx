import React, {Component} from 'react'
import TodoActions from './action.js'
import {decorate as mixin} from 'react-mixin'

@mixin(TodoActions)
export default class Header extends Component {

    onKeyDown (e) {
        let text = e.target.value.trim()
        if (e.which === 13 && text) {
            this.addItem(text)
            e.target.value = ''
        }
    }

    onChange (e) {
        let checked = e.target.checked
        this.toggleAll(checked)
    }

    render () {
        return (
            <header>
                <h1>Todos</h1>
                <input
                    className="new-todo"
                    onKeyDown={this.onKeyDown.bind(this)}
                    placeholder="What needs to be done?"
                />
                <input
                    className="toggle-all"
                    type="checkbox"
                    checked={this.props.remainItemsCount === 0}
                    onChange={this.onChange.bind(this)}
                />
            </header>
        )
    }
}