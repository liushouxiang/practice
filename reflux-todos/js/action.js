import Reflux from 'reflux'

let Actions = Reflux.createActions([
    'addItem',
    'removeItem',
    'updateItem',
    'toggleItem',
    'toggleAll',
    'clearCompletedItems'
])

export default Actions