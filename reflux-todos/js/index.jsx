require('../css/index.css')

import React from 'react'
import {render} from 'react-dom'
import {Router, Route, browserHistory} from 'react-router'
import TodoApp from './todoApp.jsx'

let container = document.querySelector('.todoapp')

render((
    <Router history={browserHistory}>
        <Route path='/' filter='all' component={TodoApp} />
        <Route path='/active' filter='active' component={TodoApp} />
        <Route path='/completed' filter='completed' component={TodoApp} />
    </Router>
), container)