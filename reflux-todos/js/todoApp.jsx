import React, {Component} from 'react'
import Reflux from 'reflux'
import TodoStore from './store.js'
import Header from './header.jsx'
import TodoItem from './todoItem.jsx'
import Toolbar from './toolbar.jsx'
import {decorate as mixin} from 'react-mixin'

@mixin(Reflux.connect(TodoStore, 'list'))
export default class TodoApp extends Component {

    getListByFilter () {
        let list = this.state.list
        let filter = this.props.route.filter
        if (filter === 'all') {
            return list
        }
        else if (filter === 'completed') {
            return list.filter(item => item.completed)
        }
        else {
            return list.filter(item => !item.completed)
        }
    }

    getRemainItemsCount () {
        let count = 0
        this.state.list.forEach(item => {
            if (!item.completed) {
                count ++
            }
        })
        return count
    }

    render () {
        let list = this.getListByFilter()
        let remainItemsCount = this.getRemainItemsCount()
        let completedItemsCount = this.state.list.length - remainItemsCount

        let listItems = list.map(item => (<TodoItem data={item} key={item.id} />))

        return (
            <div>
                <Header remainItemsCount={remainItemsCount} />
                <ul className="todo-list">
                    {listItems}
                </ul>
                <Toolbar
                    remainItemsCount={remainItemsCount}
                    completedItemsCount={completedItemsCount}
                />
            </div>
        )
    }
}