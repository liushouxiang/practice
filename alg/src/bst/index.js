/**
* 数据结构：二叉查找树(BST)
*/

class Node {
    constructor(data) {
        this.data = data;
        this._left = null;
        this._right = null;
        this._parent = null;
    }
}

class BST {
    constructor() {
        this.root = null;
    }

    insert(data) {
        let node = new Node(data);
        if (this.root === null) {
            this.root = node;
        }
        else {
            let current = this.root;
            while (true) {
                if (data < current.data) {
                    if (current._left === null) {
                        current._left = node;
                        node._parent = current;
                        break;
                    }
                    current = current._left;
                }
                else {
                    if (current._right === null) {
                        current._right = node;
                        node._parent = current;
                        break;
                    }
                    current = current._right;
                }
            }
        }
    }

    remove(data) {
        let node = this.find(data);
        let removedNode;
        let childNode;
        if (node === null) {
            return false;
        }
        if (node._left === null || node._right === null) {
            removedNode = node;
        }
        else {
            removedNode = this._findSuccessor(node);
        }

        if (removedNode._left !== null) {
            childNode = removedNode._left;
        }
        else {
            childNode = removedNode._right;
        }

        if (childNode !== null) {
            childNode._parent = removedNode._parent;
        }

        if (removedNode._parent === null) {
            this.root = childNode;
        }
        else if (removedNode._parent._left === removedNode) {
            removedNode._parent._left = childNode;
        }
        else {
            removedNode._parent._right = childNode;
        }

        if (removedNode !== node) {
            node.data = removedNode.data;
        }

        return removedNode;
    }

    // 查找节点的后继
    // 后继：节点的右子树中值最小的节点
    _findSuccessor(node) {
        let current = node._right;
        while(current._left) {
            current = current._left;
        }
        return current;
    }

    find(data) {
        let node;
        let current = this.root;
        while(true) {
            if (current === null) {
                break;
            }
            if (current.data === data) {
                node = current;
                break;
            }
            else if (current.data > data) {
                current = current._left;
            }
            else {
                current = current._right;
            }
        }
        return node;
    }

    // 中序遍历
    inOrderWalk(root, ret) {
        let node = root;
        if (node !== null) {
            this.inOrderWalk(node._left, ret);
            ret.push(node.data);
            this.inOrderWalk(node._right, ret);
        }
        return ret;
    }

    // 先序遍历
    preOrderWalk(root, ret) {
        let node = root;
        if (node !== null) {
            ret.push(node.data);
            this.preOrderWalk(node._left, ret);
            this.preOrderWalk(node._right, ret);
        }
        return ret;
    }

    // 后序遍历
    postOrderWalk(root, ret) {
        let node = root;
        if (node !== null) {
            this.postOrderWalk(node._left, ret);
            this.postOrderWalk(node._right, ret);
            ret.push(node.data);
        }
        return ret;
    }

    // 传说中的翻转二叉树
    invert(root) {
        let node = root;
        if (node !== null) {
            this.invert(node._left);
            this.invert(node._right);
            this._swap(node);
        }
    }

    //  不使用递归的版本，用栈模拟递归
    invertByStack(root) {
        let node = root;
        let stack = [];
        let tmp;
        while (stack.length || node !== null) {
            while (node !== null) {
                stack.push(node);
                node = node._left;
            }
            node = stack.pop();
            tmp = node;
            node = node._right;
            this._swap(tmp);
        }
    }

    _swap(node) {
        let tmp = node._left;
        node._left = node._right;
        node._right = tmp;
    }

}

export default BST;
