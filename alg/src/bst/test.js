import BST from './index';

let bst = new BST();

bst.insert(6);
bst.insert(4);
bst.insert(2);
bst.insert(5);
bst.insert(8);
bst.insert(7);
bst.insert(9);


let arr = [];
bst.preOrderWalk(bst.root, arr);
console.log(arr);

arr = [];
bst.invert(bst.root);
bst.preOrderWalk(bst.root, arr);
console.log(arr);
