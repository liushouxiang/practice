/**
* 数据结构：散列表
*/

const SIZE = 137;
const H = 31;

// 霍纳算法计算hash值
function getHashKey(key) {
    key = key.toString();
    let total = 0;
    for (let i = 0; i < key.length; i ++) {
        total += total * H + key.charCodeAt(i);
    }
    return parseInt(total % SIZE, 10);
}

// 基本散列表，通过链接法实现
class HashTable {
    constructor() {
        this._size = 0;
        this._datastore = new Array(SIZE);
        // 存储当前迭代元素的在二维数组中的位置
        this._point = {
            xIndex: -1,
            yIndex: -1,
            initialized: false
        };
    }

    set(key, value) {
        let hashKey = getHashKey(key);
        let exsits = false;
        let arr;
        if (arr = this._datastore[hashKey]) {
            for (let i = 0; i < arr.length; i += 2) {
                if (arr[i] === key) {
                    arr[i + 1] = value;
                    exsits = true;
                    break;
                }
            }
            if (!exsits) {
                this._datastore[hashKey].concat([key, value]);
            }
        }
        else {
            this._datastore[hashKey] = [key, value];
        }
        this._size ++;
    }

    get(key) {
        let hashKey = getHashKey(key);
        let arr = this._datastore[hashKey];
        let value;
        if (arr) {
            for (let i = 0; i < arr.length; i += 2) {
                if (arr[i] === key) {
                    value = arr[i + 1];
                    break;
                }
            }
        }
        return value;
    }

    remove(key) {
        let hashKey = getHashKey(key);
        let arr = this._datastore[hashKey];
        let exsits = false;
        if (arr) {
            for (let i = 0; i < arr.length; i ++) {
                if (arr[i] === key) {
                    arr.splice(i, 2);
                    exsits = true;
                    break;
                }
            }
        }
        return exsits;
    }

    size() {
        return this._size;
    }

    //  部署ES6迭代器接口
    [Symbol.iterator]() {
        return this;
    }

    next() {
        let ds = this._datastore;
        let pt = this._point;
        let done = false;
        if (pt.xIndex === -1 && pt.initialized) {
            done = true;
        }
        // 初次调用迭代器，将指针指向第一个元素位置
        if (!pt.initialized) {
            this._walk();
            pt.initialized = true;
        }
        let ret = {
            done: done,
            value: done ? undefined : ds[pt.xIndex][pt.yIndex + 1]
        };
        this._walk();
        return ret;
    }

    _walk() {
        let ds = this._datastore;
        let pt = this._point;
        if (ds[pt.xIndex] && pt.yIndex < ds[pt.xIndex].length - 2) {
            pt.yIndex += 2;
        }
        else {
            let exsits = false;
            for (let i = pt.xIndex + 1; i < ds.length; i ++) {
                if (ds[i]) {
                    pt.xIndex = i;
                    pt.yIndex = 0;
                    exsits = true;
                    break;
                }
            }
            if (!exsits) {
                pt.xIndex = -1;
                pt.yIndex = -1;
            }
        }
        this._point = pt;
    }
}

// 线性探测法实现
class LinearHashTable {

}

export default HashTable;
export {LinearHashTable};
