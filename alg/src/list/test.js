import List from './index';

let list = new List();

list.append(1);
list.append(2);
list.append(3);
list.append(4);

for (let value of list) {
    console.log(value);
}

list.resetPoint();
for (let value of list) {
    console.log(value);
}
