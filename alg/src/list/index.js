/**
* 数据结构：列表
*/

class List {
    constructor() {
        this._datastore = [];
        this._size = 0;
        this._point = 0;
    }

    append(item) {
        this._size ++;
        this._datastore.push(item);
    }

    insert(item, pos) {
        this._size ++;
        this._datastore.splice(pos, 0, item);
    }

    remove(item) {
        let index = this._datastore.indexOf(item);
        if (index !== -1) {
            this._datastore.splice(index, 1);
            this._size --;
        }
    }

    length() {
        return this._size;
    }

    next() {
        let item = this._datastore[this._point];
        let done = false;
        if (this._point >= this._size) {
            done = true;
        }
        else {
            this._point ++;
            done = false;
        }

        return {
            done: done,
            value: item
        };
    }

    prev() {
        if (this._point > 0) {
            this._point --;
        }
    }

    moveTo(pos) {
        this._point = pos;
    }

    getElement() {
        return this._datastore[this._point];
    }

    getPoint() {
        return this._point;
    }

    resetPoint() {
        this._point = 0;
    }

    clear() {
        this._datastore = [];
        this._point = 0;
        this._size = 0;
    }

    // 部署ES6 Iterator接口
    [Symbol.iterator]() {
        return this;
    }
}

export default List;
