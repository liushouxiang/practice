import Queue from './index';

let queue = new Queue(3);

queue.enqueue(1);
queue.enqueue(2);
queue.enqueue(3);
console.log(queue.dequeue());
queue.enqueue(4);
console.log(queue.dequeue());
console.log(queue.dequeue());
console.log(queue.dequeue());
try {
    console.log(queue.dequeue());
}
catch(e) {
    console.log(e.toString());
}
