'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

/**
* 队列
*/

var Queue = function () {
    function Queue(size) {
        _classCallCheck(this, Queue);

        this._datastore = new Array(size + 1);
        this._size = size;
        this._head = 0;
        this._tail = 0;
    }

    _createClass(Queue, [{
        key: 'enqueue',
        value: function enqueue(item) {
            var tail = this._tail + 1 > this._size ? 0 : this._tail + 1;
            // 队列上溢
            if (tail === this._head) {
                throw new Error('queue overflow');
            }
            this._datastore[this._tail] = item;
            this._tail = tail;
        }
    }, {
        key: 'dequeue',
        value: function dequeue() {
            // 队列下溢
            if (this._tail === this._head) {
                throw new Error('queue underflow');
            }
            var ret = this._datastore[this._head];
            if (this._head === this._size) {
                this._head = 0;
            } else {
                this._head += 1;
            }
            return ret;
        }
    }]);

    return Queue;
}();

exports.default = Queue;