"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

/**
* 数据结构：二叉查找树(BST)
*/

var Node = function Node(data) {
    _classCallCheck(this, Node);

    this.data = data;
    this._left = null;
    this._right = null;
    this._parent = null;
};

var BST = function () {
    function BST() {
        _classCallCheck(this, BST);

        this.root = null;
    }

    _createClass(BST, [{
        key: "insert",
        value: function insert(data) {
            var node = new Node(data);
            if (this.root === null) {
                this.root = node;
            } else {
                var current = this.root;
                while (true) {
                    if (data < current.data) {
                        if (current._left === null) {
                            current._left = node;
                            node._parent = current;
                            break;
                        }
                        current = current._left;
                    } else {
                        if (current._right === null) {
                            current._right = node;
                            node._parent = current;
                            break;
                        }
                        current = current._right;
                    }
                }
            }
        }
    }, {
        key: "remove",
        value: function remove(data) {
            var node = this.find(data);
            var removedNode = void 0;
            var childNode = void 0;
            if (node === null) {
                return false;
            }
            if (node._left === null || node._right === null) {
                removedNode = node;
            } else {
                removedNode = this._findSuccessor(node);
            }

            if (removedNode._left !== null) {
                childNode = removedNode._left;
            } else {
                childNode = removedNode._right;
            }

            if (childNode !== null) {
                childNode._parent = removedNode._parent;
            }

            if (removedNode._parent === null) {
                this.root = childNode;
            } else if (removedNode._parent._left === removedNode) {
                removedNode._parent._left = childNode;
            } else {
                removedNode._parent._right = childNode;
            }

            if (removedNode !== node) {
                node.data = removedNode.data;
            }

            return removedNode;
        }

        // 查找节点的后继
        // 后继：节点的右子树中值最小的节点

    }, {
        key: "_findSuccessor",
        value: function _findSuccessor(node) {
            var current = node._right;
            while (current._left) {
                current = current._left;
            }
            return current;
        }
    }, {
        key: "find",
        value: function find(data) {
            var node = void 0;
            var current = this.root;
            while (true) {
                if (current === null) {
                    break;
                }
                if (current.data === data) {
                    node = current;
                    break;
                } else if (current.data > data) {
                    current = current._left;
                } else {
                    current = current._right;
                }
            }
            return node;
        }

        // 中序遍历

    }, {
        key: "inOrderWalk",
        value: function inOrderWalk(root, ret) {
            var node = root;
            if (node !== null) {
                this.inOrderWalk(node._left, ret);
                ret.push(node.data);
                this.inOrderWalk(node._right, ret);
            }
            return ret;
        }

        // 先序遍历

    }, {
        key: "preOrderWalk",
        value: function preOrderWalk(root, ret) {
            var node = root;
            if (node !== null) {
                ret.push(node.data);
                this.preOrderWalk(node._left, ret);
                this.preOrderWalk(node._right, ret);
            }
            return ret;
        }

        // 后序遍历

    }, {
        key: "postOrderWalk",
        value: function postOrderWalk(root, ret) {
            var node = root;
            if (node !== null) {
                this.postOrderWalk(node._left, ret);
                this.postOrderWalk(node._right, ret);
                ret.push(node.data);
            }
            return ret;
        }

        // 传说中的翻转二叉树

    }, {
        key: "invert",
        value: function invert(root) {
            var node = root;
            if (node !== null) {
                this.invert(node._left);
                this.invert(node._right);
                this._swap(node);
            }
        }

        //  不使用递归的版本，用栈模拟递归

    }, {
        key: "invertByStack",
        value: function invertByStack(root) {
            var node = root;
            var stack = [];
            var tmp = void 0;
            while (stack.length || node !== null) {
                while (node !== null) {
                    stack.push(node);
                    node = node._left;
                }
                node = stack.pop();
                tmp = node;
                node = node._right;
                this._swap(tmp);
            }
        }
    }, {
        key: "_swap",
        value: function _swap(node) {
            var tmp = node._left;
            node._left = node._right;
            node._right = tmp;
        }
    }]);

    return BST;
}();

exports.default = BST;