'use strict';

var _index = require('./index');

var _index2 = _interopRequireDefault(_index);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var bst = new _index2.default();

bst.insert(6);
bst.insert(4);
bst.insert(2);
bst.insert(5);
bst.insert(8);
bst.insert(7);
bst.insert(9);

var arr = [];
bst.preOrderWalk(bst.root, arr);
console.log(arr);

arr = [];
bst.invertByStack(bst.root);
bst.preOrderWalk(bst.root, arr);
console.log(arr);