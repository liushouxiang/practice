"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

/**
* 数据结构：散列表
*/

var SIZE = 137;
var H = 31;

// 霍纳算法计算hash值
function getHashKey(key) {
    key = key.toString();
    var total = 0;
    for (var i = 0; i < key.length; i++) {
        total += total * H + key.charCodeAt(i);
    }
    return parseInt(total % SIZE, 10);
}

// 基本散列表，通过链接法实现

var HashTable = function () {
    function HashTable() {
        _classCallCheck(this, HashTable);

        this._size = 0;
        this._datastore = new Array(SIZE);
        // 存储当前迭代元素的在二维数组中的位置
        this._point = {
            xIndex: -1,
            yIndex: -1,
            initialized: false
        };
    }

    _createClass(HashTable, [{
        key: "set",
        value: function set(key, value) {
            var hashKey = getHashKey(key);
            var exsits = false;
            var arr = void 0;
            if (arr = this._datastore[hashKey]) {
                for (var i = 0; i < arr.length; i += 2) {
                    if (arr[i] === key) {
                        arr[i + 1] = value;
                        exsits = true;
                        break;
                    }
                }
                if (!exsits) {
                    this._datastore[hashKey].concat([key, value]);
                }
            } else {
                this._datastore[hashKey] = [key, value];
            }
            this._size++;
        }
    }, {
        key: "get",
        value: function get(key) {
            var hashKey = getHashKey(key);
            var arr = this._datastore[hashKey];
            var value = void 0;
            if (arr) {
                for (var i = 0; i < arr.length; i += 2) {
                    if (arr[i] === key) {
                        value = arr[i + 1];
                        break;
                    }
                }
            }
            return value;
        }
    }, {
        key: "remove",
        value: function remove(key) {
            var hashKey = getHashKey(key);
            var arr = this._datastore[hashKey];
            var exsits = false;
            if (arr) {
                for (var i = 0; i < arr.length; i++) {
                    if (arr[i] === key) {
                        arr.splice(i, 2);
                        exsits = true;
                        break;
                    }
                }
            }
            return exsits;
        }
    }, {
        key: "size",
        value: function size() {
            return this._size;
        }

        //  部署ES6迭代器接口

    }, {
        key: Symbol.iterator,
        value: function value() {
            return this;
        }
    }, {
        key: "next",
        value: function next() {
            var ds = this._datastore;
            var pt = this._point;
            var done = false;
            if (pt.xIndex === -1 && pt.initialized) {
                done = true;
            }
            // 初次调用迭代器，将指针指向第一个元素位置
            if (!pt.initialized) {
                this._walk();
                pt.initialized = true;
            }
            var ret = {
                done: done,
                value: done ? undefined : ds[pt.xIndex][pt.yIndex + 1]
            };
            this._walk();
            return ret;
        }
    }, {
        key: "_walk",
        value: function _walk() {
            var ds = this._datastore;
            var pt = this._point;
            if (ds[pt.xIndex] && pt.yIndex < ds[pt.xIndex].length - 2) {
                pt.yIndex += 2;
            } else {
                var exsits = false;
                for (var i = pt.xIndex + 1; i < ds.length; i++) {
                    if (ds[i]) {
                        pt.xIndex = i;
                        pt.yIndex = 0;
                        exsits = true;
                        break;
                    }
                }
                if (!exsits) {
                    pt.xIndex = -1;
                    pt.yIndex = -1;
                }
            }
            this._point = pt;
        }
    }]);

    return HashTable;
}();

// 线性探测法实现


var LinearHashTable = function LinearHashTable() {
    _classCallCheck(this, LinearHashTable);
};

exports.default = HashTable;
exports.LinearHashTable = LinearHashTable;