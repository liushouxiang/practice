'use strict';

var _index = require('./index');

var _index2 = _interopRequireDefault(_index);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
* 使用栈实现进制转换
* 将十进制数num转换为base进制的数
*/
function sysConvert(num, base) {
    var stack = new _index2.default();
    var ret = '';
    do {
        stack.push(num % base);
    } while (num = Math.floor(num / base));
    while (stack.length() > 0) {
        ret += stack.pop();
    }
    return ret;
}

console.log(sysConvert(100, 2));

/**
* 使用栈检测字符串是否为回文
*/
function isPlalindrome(str) {
    str = '' + str;
    var stack = new _index2.default();
    var ret = true;
    for (var i = 0; i < str.length; i++) {
        stack.push(str[i]);
    }
    for (var _i = 0; _i < str.length; _i++) {
        if (str[_i] !== stack.pop()) {
            ret = false;
            break;
        }
    }
    return ret;
}

console.log(isPlalindrome('racecar'));
console.log(isPlalindrome('abc'));

// 使用栈实现阶乘
function factorial(num) {
    var stack = new _index2.default();
    while (num > 0) {
        stack.push(num--);
    }
    var ret = 1;
    while (stack.length() > 0) {
        ret *= stack.pop();
    }
    return ret;
}

console.log(factorial(5));