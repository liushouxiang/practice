var gulp = require('gulp');
var watch = require('gulp-watch');
var babel = require('gulp-babel');

gulp.task('dev', function () {
    return gulp.src('./src/**/*.js')
        .pipe(watch('./src/**/*.js'))
        .pipe(babel())
        .pipe(gulp.dest('./output'));
});


gulp.task('default', ['dev']);