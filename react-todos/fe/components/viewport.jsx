var React = require('react');

var Viewport = React.createClass({
	render: function () {
		return (
			<html>
				<head>
					<meta charset="UTF-8" />
					<link rel="stylesheet" href="/static/css/todos.css" />
					<title>React todolist</title>
				</head>
				<body>
					<div id="content" dangerouslySetInnerHTML={{__html: this.props.html || ''}}></div>
					<script src="/static/js/app.js"></script>
				</body>
			</html>
		);
	}
});

module.exports = {
	create: function (Component, props) {
		var layoutProps = {};
		if (Component) {
			var html = React.createElement(Component, props || {});
			layoutProps.html = React.renderToString(html);
		}
		return React.renderToStaticMarkup(React.createElement(Viewport, layoutProps));
	}
};