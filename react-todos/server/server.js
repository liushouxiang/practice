var express = require('express');
var react = require('react');
var path = require('path');
var app = express();

app.use('/static', express.static(path.join(__dirname, 'static')));

app.use('*', function (req, res) {
	var Viewport = require('./static/components/viewport.js');
	var html = Viewport.create();
	res.setHeader('Content-Type', 'text/html');
	res.end(html);
});

app.use('/api/login', function (req, res) {
	var uname = req.body.uname;
	var pword = req.body.pword;
	var persistent = req.body.persistent;
	// TODO
})

app.listen(8888);

module.exports = app;
