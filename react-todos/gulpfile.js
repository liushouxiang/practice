var webpack = require('webpack');
var gulp = require('gulp');
var changed = require('gulp-changed');
var react = require('gulp-react');

var distServer = __dirname + '/output/';
var distFE = __dirname + '/output/static/';


gulp.task('_copyFE', function () {
	return gulp.src(['./fe/**', '!./fe/**/*.jsx'], {base: './fe/'})
				.pipe(changed(distFE))
				.pipe(gulp.dest(distFE));
});

gulp.task('copyFE', ['_copyFE'], function () {
	return gulp.src(['./fe/app.js']).pipe(gulp.dest(distFE));
});

gulp.task('copyServer', function () {
	return gulp.src('./server/**', {base: './server'})
			   .pipe(changed(distServer))
			   .pipe(gulp.dest(distServer));
});

gulp.task('parseJSX', function () {
	return gulp.src('./fe/**/*.jsx', {base: './fe/'})
			   .pipe(changed(distFE))
			   .pipe(react())
			   .pipe(gulp.dest(distFE));
});

gulp.task('webpack', ['copyFE', 'parseJSX'], function () {
	return webpack({
		context: __dirname + '/output/static',
		entry: {
			app: './app.js'
			// lib: ['react']
		},
		output: {
			path: __dirname + '/output/static/js',
			publicPath: '/static/js/',
			filename: '[name].js',
			chunkFilename: 'page-[id].js'
		}
		// plugins: [
		// 	new webpack.optimize.CommonsChunkPlugin('lib', 'lib.js')
		// ]
	}, function (err, stats) {
		if (err) {
			console.log('run webpack task failure, please try again...');
		}
	});
});

gulp.task('dev', ['webpack', 'copyServer']);

gulp.task('default', ['dev'], function () {
	gulp.watch(['gulpfile.js', './fe/**', './server/**'], ['dev']);
});

//清理工作区，TODO
gulp.task('clean', function () {

});

//产品发布，TODO
gulp.task('release', function () {

});