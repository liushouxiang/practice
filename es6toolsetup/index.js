#!/usr/bin/env node

var fs = require('fs')
var path = require('path')
var chalk = require('chalk')
var spawnSync = require('child_process').spawnSync
var execSync = require('child_process').execSync
var CWD = process.cwd()

function checkIfCnpmExists() {
    var ret;
    try {
        ret = execSync('cnpm -v', {stdio: ['ignore', 'ignore', 'ignore']})
    }
    catch (e) {
        ret = false
    }
    return !!ret;
}

function checkIfNpmInit() {
    var packageFile = path.join(CWD, 'package.json')
    return fs.existsSync(packageFile)
}

function getDependences() {

    var depsConfigFile = path.join(__dirname, 'dependencies.json')
    var config = JSON.parse(fs.readFileSync(depsConfigFile))

    var deps = config.deps
    var devDeps = config.devDeps
    var extraDeps = config.extraDeps
    var extraLibs = process.argv.slice(2)
    var babelPresets = config.babelPresets
    var babelPlugins = config.babelPlugins

    extraLibs.forEach(function (name) {
        var extra = extraDeps[name];
        if (extra) {
            deps = deps.concat(extra.deps || [])
            devDeps = devDeps.concat(extra.devDeps || [])
            babelPresets = babelPresets.concat(extra.babelPresets || [])
        }
        else {
            devDeps.push(name)
        }
    })

    return {
        deps: deps,
        devDeps: devDeps,
        babelrc: {
            presets: babelPresets,
            plugins: babelPlugins
        }
    }
}

var config = getDependences()
var installDepsArgs = ['install', '--save'].concat(config.deps)
var installDevDepsArgs = ['install', '--save-dev'].concat(config.devDeps)
var npmCmd = checkIfCnpmExists() ? 'cnpm' : 'npm'
var genBabelrcCmd = 'echo \'' + JSON.stringify(config.babelrc) + '\' > ' + path.join(CWD, '.babelrc')


if (checkIfNpmInit()) {
    var babelrc = JSON.stringify()
    spawnSync(npmCmd, installDepsArgs, {stdio: 'inherit', cwd: CWD})
    spawnSync(npmCmd, installDevDepsArgs, {stdio: 'inherit', cwd: CWD})
    execSync(genBabelrcCmd)
    console.log(chalk.green('Done!'))
    process.exit()
}
else {
    console.log('please run ' + chalk.red('npm init') + ' first...')
    process.exit()
}