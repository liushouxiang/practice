import React, {Component} from 'react'
import {ENTER_KEY} from './config.js'

export default class Header extends Component {

    onKeyDown (e) {
        let text = e.target.value.trim()
        if (e.which === ENTER_KEY && text) {
            this.props.handleSubmit(text)
            e.target.value = ''
        }
    }

    onChange (e) {
        let checked = e.target.checked
        this.props.handleToggleAll(checked)
    }

    render () {
        let stylesheet = this.props.stylesheet
        return (
            <header>
                <h1>Todos</h1>
                <input
                    className={stylesheet['new-todo']}
                    onKeyDown={this.onKeyDown.bind(this)}
                    placeholder="What needs to be done?"
                />
                <input
                    className={stylesheet['toggle-all']}
                    type="checkbox"
                    checked={this.props.remainItemsCount === 0}
                    onChange={this.onChange.bind(this)}
                />
            </header>
        )
    }
}