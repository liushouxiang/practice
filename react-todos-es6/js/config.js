export const ALL_FILTER = Symbol('all')
export const ACTIVE_FILTER = Symbol('active')
export const COMPLETED_FILTER = Symbol('completed')

export const ENTER_KEY = 13
export const ESCAPE_KEY = 27