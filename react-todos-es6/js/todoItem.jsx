import React, {Component} from 'react'
import classNames from 'classnames'

import {ESCAPE_KEY, ENTER_KEY} from './config.js'

export default class TodoItem extends Component {

    constructor (props) {
        super(props)
        this.state = {
            editing: false
        }
        this.data = this.props.data
    }
    
    onToggle (e) {
        let checked = e.target.checked
        this.props.handleToggle(this.data.id, checked)
    }

    onDestroy () {
        this.props.handleDestroy(this.data.id)
    }

    onEditing () {
        this.setState({editing: true}, () => this.refs.editField.focus())
    }

    onKeyDown (e) {
        if (e.which === ESCAPE_KEY) {
            this.setState({
                editing: false
            })
        }
        else if (e.which === ENTER_KEY) {
            this.setState({
                editing: false
            })
            this.props.handleUpdate(this.data.id, e.target.value)
        }
    }

    onBlur () {
        this.setState({
            editing: false
        })
    }

    render () {
        let stylesheet = this.props.stylesheet
        return (
            <li className={classNames({
                completed: this.data.complete,
                editing: this.state.editing
            })}>
                <div className={stylesheet.view}>
                    <input
                        className={stylesheet.toggle}
                        onChange={this.onToggle.bind(this)}
                        checked={this.data.complete}
                        type="checkbox"
                    />
                    <label
                        onDoubleClick={this.onEditing.bind(this)}
                    >
                        {this.data.text}
                    </label>
                    <button
                        className={stylesheet.destroy}
                        onClick={this.onDestroy.bind(this)}
                    >
                    </button>
                </div>
                <input
                    className={stylesheet.edit}
                    ref="editField"
                    type="text"
                    defaultValue={this.data.text}
                    onKeyDown={this.onKeyDown.bind(this)}
                    onBlur={this.onBlur.bind(this)}
                />
            </li>
        )
    }
}