const storage = localStorage

const timestamp = () => (new Date()).getTime()

export default class Store {
    
    constructor (name) {
        this.name = name
        this.data = this.fetch()
    }

    insert (text = '', id = timestamp(), complete = false) {
        this.data.unshift({
            id: id,
            text: text,
            complete: complete
        })
        this.save()
        this.notify()
    }

    update (newItem) {
        for (let item of this.data) {
            if (item.id === newItem.id) {
                Object.assign(item, newItem)
                this.save()
                this.notify()
                return true
            }
        }
        return false
    }

    remove (key, ...args) {
        let data = []
        for (let item of this.data) {
            if (!args.includes(item[key])) {
                data.push(item)
            }
        }
        if (data.length < this.data.length) {
            this.data = data
            this.save()
            this.notify()
            return true
        }
        return false
    }

    read (key, ...args) {
        if (key) {
            let data = []
            for (let item of this.data) {
                if (args.includes(item[key])) {
                    data.push(item)
                }
            }
            return data
        }
        else {
            return this.data || []
        }
    }

    save () {
        storage.setItem(this.name, JSON.stringify(this.data))
    }

    fetch () {
        return JSON.parse(storage.getItem(this.name)) || []
    }

    notify () {
        this.callbacks.forEach((cb) => {
            cb(this.data)
        })
    }

    subscribe (cb) {
        this.callbacks = this.callbacks || []
        this.callbacks.push(cb)
    }

}