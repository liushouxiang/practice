module.exports = {
    entry: {
        aio: './js/index.js'
    },

    output: {
        path: __dirname + '/.output',
        filename: '[name].js',
        publicPath: '/static'
    },

    module: {
        loaders: [
            {
                test: /\.css/,
                loaders: ['style', 'css']
            },
            {
                test: /\.jsx?$/,
                exclude: /node_modules/,
                loader: 'babel'
            }
        ]
    }
};