/**
 * Decorator pattern示例
 * */

export class Dialog {

    close () {
        console.log('close the dialog')
    }

    render () {
        console.log('the base Dialog')
    }

}

/**
 * 装饰器必须和被装饰类具备相同的接口
 * */
export class DialogDecorate extends Dialog {

    constructor (dialog) {
        super(dialog)
        this.dialog = dialog
    }

}

export class DialogWithBorder extends DialogDecorate {

    constructor (dialog) {
        super(dialog)
    }

    /**
     * override渲染方法
     * */
    render () {
        this.dialog.render()
        console.log(`decorated dialog with border`)
    }

}

export class DialogWithScrollbar extends DialogDecorate {

    constructor (dialog) {
        super(dialog)
    }

    render () {
        this.dialog.render()
        console.log('decorated dialog with scrolling bar')
    }

}

/**
 * decorator的优势: 随意组合装饰器,实现不同的组合效果
 * let dialogWidthBorderAndScrollbar = new DialogWithScrollbar(new DialogWithBorder(new Dialog()))
 * */

