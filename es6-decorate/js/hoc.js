import React, {Component} from 'react'
import ReactDOM from 'react-dom'

const Enhance = ComposedComponent => class EnhanceLazyload extends Component {

    constructor () {
        super()
        this.state = {visible : false}
        this.checkIfVisible = this.checkIfVisible.bind(this)
        window.addEventListener('scroll', this.checkIfVisible)
    }

    componentDidMount () {
        this.checkIfVisible()
    }

    checkIfVisible () {
        let wrapper = this.refs.lazyWrapper
        let rect = wrapper.getBoundingClientRect()
        let innerHeight = window.innerHeight
        if (rect.top < innerHeight && rect.bottom > 0) {
            window.removeEventListener('scroll', this.checkIfVisible)
            this.setState({visible: true})
        }
    }

    render () {
        var visible = this.state.visible
        return (
            <div ref="lazyWrapper">
                <ComposedComponent ref="lazyloadComponent" {...this.props} visible={visible} />
            </div>
        )
    }

}

@Enhance
class Panel extends Component {

    render () {
        if (!this.props.visible) {
            return <Loading />
        }
        return <div>实际内容</div>
    }

}

class Loading extends Component {

    render () {
        return <div>loading...</div>
    }

}

// export const LazyloadPanel = Enhance(Panel)
export const LazyloadPanel = Panel
